package org.homi.interconsultas.network;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.homi.interconsultas.BuildConfig;
import org.homi.interconsultas.presentation.dashboard.domain.Interconsulta;
import org.homi.interconsultas.presentation.login.domain.User;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by miguelarroyo on 8/24/17.
 */

public interface ApiService {

    String BASE_URL = "http://fhmexterno.fundacionhomi.org.co/WebApp/";

    @GET("api/Login")
    Call<User> login(@Query("user") String user, @Query("pass") String password);

    @GET("api/Interconsulta")
    Call<List<Interconsulta>>getInterconsulta(@Query("cod_especialidad") String codigoEspecialidad);

    class Factory {

        public static ApiService create() {
            HttpLoggingInterceptor httpLoggingInterceptor = null;
            if (BuildConfig.DEBUG) {
                httpLoggingInterceptor = new HttpLoggingInterceptor();
                httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            }

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.readTimeout(30, TimeUnit.SECONDS);
            builder.connectTimeout(30, TimeUnit.SECONDS);

            builder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(@NonNull Chain chain) throws IOException {
                    Request request = chain.request()
                            .newBuilder()
                            .build();
                    return chain.proceed(request);
                }
            });
            if (httpLoggingInterceptor != null) {
                builder.addInterceptor(httpLoggingInterceptor);
            }

            Gson customGson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                                               .setVersion(1.0)
                                               .setPrettyPrinting()
                                               .create();

            Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                    .client(builder.build())
                    .addConverterFactory(GsonConverterFactory.create(customGson))
                    .build();

            return retrofit.create(ApiService.class);
        }
    }
}
