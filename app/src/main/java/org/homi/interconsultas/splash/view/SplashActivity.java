package org.homi.interconsultas.splash.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;

import org.homi.interconsultas.R;
import org.homi.interconsultas.presentation.dashboard.view.DashboardActivity;
import org.homi.interconsultas.presentation.login.view.LoginActivity;
import org.homi.interconsultas.splash.SplashContract;
import org.homi.interconsultas.splash.presenter.SplashPresenter;

/**
 * Created by leydisli on 10/09/2017.
 */

public class SplashActivity extends AppCompatActivity implements SplashContract.SplashView {

    SplashPresenter mSplashPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mSplashPresenter = new SplashPresenter(this);
        mSplashPresenter.attachView(this);
        mSplashPresenter.verifyNewVersion();
    }

    @Override
    public void showProgressBar() {

    }

    @Override
    public void hideProgressBar() {

    }

    @Override
    public void showError(String errorMessage) {

    }

    @Override
    public void showError(@StringRes int stringResourceId) {

    }

    @Override
    public void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        finish();
    }

    @Override
    public void goToDashboard() {
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);

        finish();
    }
}
