package org.homi.interconsultas.splash.presenter;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;

import com.google.gson.Gson;

import org.homi.interconsultas.base.BaseContract;
import org.homi.interconsultas.presentation.login.domain.User;
import org.homi.interconsultas.splash.SplashContract.Presenter;
import org.homi.interconsultas.splash.SplashContract.SplashView;
import org.homi.interconsultas.utils.PreferenceUtil;

/**
 * Created by leydisli on 10/09/2017.
 */

public class SplashPresenter implements Presenter<SplashView> {

    private Context mContext;
    private SplashView mSplashView;
    private PreferenceUtil mPreferenceUtil;

    public SplashPresenter(Context context) {
        this.mContext = context;
        mPreferenceUtil = new PreferenceUtil(context);
    }

    @Override
    public void verifySession() {
        String currentUserJson = mPreferenceUtil.readString("USER");

        if (!TextUtils.isEmpty(currentUserJson)) {
            User currentUser = new Gson().fromJson(currentUserJson, User.class);

            if (currentUser != null) {
                mSplashView.goToDashboard();
            } else {
                mSplashView.goToLogin();
            }
        } else {
            mSplashView.goToLogin();
        }
    }

    @Override
    public void verifyNewVersion() {
        new Handler().postDelayed(() -> verifySession(), 3000);
    }

    @Override
    public void attachView(BaseContract.BaseView view) {
        mSplashView = (SplashView) view;
    }

    @Override
    public void detachView() {

    }
}
