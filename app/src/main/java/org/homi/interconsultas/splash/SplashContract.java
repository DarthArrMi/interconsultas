package org.homi.interconsultas.splash;

import org.homi.interconsultas.base.BaseContract.BasePresenter;
import org.homi.interconsultas.base.BaseContract.BaseView;

/**
 * Created by leydisli on 10/09/2017.
 */

public interface SplashContract {

    interface SplashView extends BaseView {

       void goToLogin();

        void goToDashboard();
    }

    interface Presenter<V extends SplashView> extends BasePresenter {

        void verifySession();

        void verifyNewVersion();
    }
}
