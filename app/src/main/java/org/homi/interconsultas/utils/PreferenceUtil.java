package org.homi.interconsultas.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by root on 4/01/17.
 */

public class PreferenceUtil {
    private Context mContext;
    private SharedPreferences.Editor mEditor;

    private SharedPreferences mSharedPreferences;

    private static final String PREF_NAME = "geotraq_preferences.xml";

    public PreferenceUtil(Context context) {
        this.mContext = context;

        mSharedPreferences = context.getSharedPreferences("carrier-billing-sdk", Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public boolean removeValue(String key) {
        mEditor.remove(key);
        return mEditor.commit();
    }

    public boolean writeBoolean(String key, boolean value) {
        mEditor.putBoolean(key, value);
        return mEditor.commit();
    }

    public boolean readBoolean(String key) {
        return mSharedPreferences.getBoolean(key, false);
    }

    public boolean writeString(String key, String value) {
        mEditor.putString(key, value);
        return mEditor.commit();
    }

    public String readString(String key) {
        return mSharedPreferences.getString(key, "");
    }

    public boolean writeFloat(String key, float value) {
        mEditor.putFloat(key, value);
        return mEditor.commit();
    }

    public float readFloat(String key) {
        return mSharedPreferences.getFloat(key, -1f);
    }

    public boolean writeInt(String key, int value) {
        mEditor.putInt(key, value);
        return mEditor.commit();
    }

    public int readInt(String key) {
        return mSharedPreferences.getInt(key, -1);
    }

    public boolean writeLong(String key, long value) {
        mEditor.putLong(key, value);
        return mEditor.commit();
    }

    public long readLong(String key) {
        return mSharedPreferences.getLong(key, -1l);
    }

}
