package org.homi.interconsultas.presentation.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

/**
 * Loading Dialog, lets know the user that we're doing something on background.
 *
 * @author miguel.arroyo (miguel.arroyo@movile.com).
 */
public class LoadingDialog extends DialogFragment {

    public static final String FRAGMENT_TAG = "loading_dialog";
    private static final String KEY_LABEL = "key_label";

    private String mLabel;

    /**
     * Creates a new instance of this {@link android.support.v4.app.Fragment} with the given label.
     * Use this method, rather than the default constructor, to provide this label.
     *
     * @param label The label shown when the {@link android.support.v4.app.Fragment} is visible.
     * @return A new instance of {@link LoadingDialog}.
     */
    public static LoadingDialog newInstance(@NonNull String label) {
        Bundle args = new Bundle();
        args.putString(KEY_LABEL, label);

        LoadingDialog fragment = new LoadingDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mLabel = savedInstanceState.getString(KEY_LABEL);
        } else {
            Bundle arguments = getArguments();

            // Null-check in case someone instantiate this Fragment by using its default constructor
            if (arguments != null) {
                mLabel = arguments.getString(KEY_LABEL);
            } else {
                throw new IllegalArgumentException("No String provided for the label of the Fragment." +
                        "Instantiate the Dialog by using the method newInstance.");
            }
        }
        setCancelable(false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog progressDialog = new ProgressDialog(getActivity(), getTheme());
        progressDialog.setMessage(mLabel);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return progressDialog;
    }
}
