package org.homi.interconsultas.presentation.dashboard.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by miguelarroyo on 8/25/17.
 */

public class Interconsulta implements Parcelable {

    @SerializedName("PACIENTE")
    @Expose
    private String mPatientName;

    @SerializedName("HC")
    @Expose
    private String mHc;

    @SerializedName("CAMA")
    @Expose
    private String mBed;

    @SerializedName("FECHA_SOLICITUD")
    @Expose
    private Date mDate;

    @SerializedName("SUBESPECIALIDAD_SOLICITADA")
    @Expose
    private String mSubspecialty;

    public Interconsulta() {
    }

    public String getPatientName() {
        return mPatientName;
    }

    public void setPatientName(String pACIENTE) {
        this.mPatientName = pACIENTE;
    }

    public String getHc() {
        return mHc;
    }

    public void setHc(String hC) {
        this.mHc = hC;
    }

    public String getBed() {
        return mBed;
    }

    public void setBed(String cAMA) {
        this.mBed = cAMA;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date fECHASOLICITUD) {
        this.mDate = fECHASOLICITUD;
    }

    public String getSubspecialty() {
        return mSubspecialty;
    }

    public void setSubspecialty(String sUBESPECIALIDADSOLICITADA) {
        this.mSubspecialty = sUBESPECIALIDADSOLICITADA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Interconsulta that = (Interconsulta) o;

        if (!mPatientName.equals(that.mPatientName)) return false;
        if (!mHc.equals(that.mHc)) return false;
        if (!mBed.equals(that.mBed)) return false;
        if (!mDate.equals(that.mDate)) return false;
        return mSubspecialty.equals(that.mSubspecialty);

    }

    @Override
    public int hashCode() {
        int result = mPatientName.hashCode();
        result = 31 * result + mHc.hashCode();
        result = 31 * result + mBed.hashCode();
        result = 31 * result + mDate.hashCode();
        result = 31 * result + mSubspecialty.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Interconsulta{");
        sb.append("PatientName='").append(mPatientName).append('\'');
        sb.append(", Hc='").append(mHc).append('\'');
        sb.append(", Bed='").append(mBed).append('\'');
        sb.append(", Date='").append(mDate).append('\'');
        sb.append(", Subspecialty='").append(mSubspecialty).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mPatientName);
        dest.writeString(this.mHc);
        dest.writeString(this.mBed);
        dest.writeLong(this.mDate != null ? this.mDate.getTime() : -1);
        dest.writeString(this.mSubspecialty);
    }

    protected Interconsulta(Parcel in) {
        this.mPatientName = in.readString();
        this.mHc = in.readString();
        this.mBed = in.readString();
        long tmpMDate = in.readLong();
        this.mDate = tmpMDate == -1 ? null : new Date(tmpMDate);
        this.mSubspecialty = in.readString();
    }

    public static final Creator<Interconsulta> CREATOR = new Creator<Interconsulta>() {

        @Override
        public Interconsulta createFromParcel(Parcel source) {return new Interconsulta(source);}

        @Override
        public Interconsulta[] newArray(int size) {return new Interconsulta[size];}
    };
}
