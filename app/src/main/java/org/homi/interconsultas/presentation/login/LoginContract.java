package org.homi.interconsultas.presentation.login;

import org.homi.interconsultas.base.BaseContract.BasePresenter;
import org.homi.interconsultas.base.BaseContract.BaseView;

/**
 * @author miguel.arroyo (miguel.arroyo@movile.com).
 */
public interface LoginContract {

    interface LoginView extends BaseView {

        void loginSuccessful();
    }

    interface Presenter<V extends LoginView> extends BasePresenter {

        void login(String username, String password);
    }
}
