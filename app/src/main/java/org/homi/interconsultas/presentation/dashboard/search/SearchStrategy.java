package org.homi.interconsultas.presentation.dashboard.search;

import org.homi.interconsultas.presentation.dashboard.domain.Interconsulta;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public interface SearchStrategy {

    /**
     *
     * @param query
     * @return
     */
    ArrayList<Interconsulta> performFiltering(CharSequence query, List<Interconsulta> interconsultas);
}
