package org.homi.interconsultas.presentation.dashboard.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import org.homi.interconsultas.R;
import org.homi.interconsultas.base.BaseContract;
import org.homi.interconsultas.network.ApiService;
import org.homi.interconsultas.presentation.dashboard.DashboardContract;
import org.homi.interconsultas.presentation.dashboard.domain.Interconsulta;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by miguelarroyo on 8/25/17.
 */

public class DashboardPresenter implements DashboardContract.Presenter {

    private final String TAG = DashboardPresenter.class.getSimpleName();

    private ApiService mApiService;
    private DashboardContract.InterconsultaView mDashboardView;

    public DashboardPresenter() {
        mApiService = ApiService.Factory.create();
    }

    @Override
    public void getInterconsulta(String code) {
        mDashboardView.showProgressBar();

        Call<List<Interconsulta>> call = mApiService.getInterconsulta(code);
        call.enqueue(new Callback<List<Interconsulta>>() {
            @Override
            public void onResponse
            (@NonNull Call < List < Interconsulta >> call, @NonNull Response < List < Interconsulta >> response)
            {
                mDashboardView.hideProgressBar();

                List<Interconsulta> list = response.body();
                if (list!= null && list.size() < 0) {
                    mDashboardView.showError(R.string.interconsulta_error_lista);
                } else {
                    mDashboardView.getListInterconsulta(list);
                }
            }

            @Override
            public void onFailure (@NonNull Call < List<Interconsulta> > call, @NonNull Throwable t){
                Log.e(TAG, "Error in Interconsulta", t);
                mDashboardView.hideProgressBar();
                mDashboardView.showError(R.string.interconsulta_error_lista);
            }
        });
    }

    @Override
    public void attachView(BaseContract.BaseView view) {
        mDashboardView= (DashboardContract.InterconsultaView) view;
    }

    @Override
    public void detachView() {
        mDashboardView=null;

    }
}
