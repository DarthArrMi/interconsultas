package org.homi.interconsultas.presentation.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import org.homi.interconsultas.R;

/**
 * @author miguel.arroyo (miguel.arroyo@movile.com).
 */
public class SearchDialog extends DialogFragment {

    public static final String FRAGMENT_TAG = "loading_dialog";
    private TextInputLayout mInputLayout;
    private TextInputEditText mEditTextPin;

    public interface SearchCallbacks {

        void onQuery(String query);
    }

    private SearchCallbacks mCallbacks;

    public static SearchDialog newInstance() {
        SearchDialog fragment = new SearchDialog();
        return fragment;
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);

        if (context instanceof SearchCallbacks) {
            mCallbacks = (SearchCallbacks) context;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        LayoutInflater inflater =
                (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_dialog_search_input, null);

        mEditTextPin = (TextInputEditText) view.findViewById(R.id.pin_input_pin);
        mInputLayout = (TextInputLayout) view.findViewById(R.id.pin_input_container_pin);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setPositiveButton("Buscar", (dialog, which) -> {
            /*
             * As part of the workaround, we still need to set up a positive button to provide support
             * for older version of Android. We will override this empty behaviour later in onResume.
             */
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {
            dialog.dismiss();
        });
        builder.setView(view);

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();

        /*
         * AlertDialogs registers a common button handler to all the Buttons added during onCreate
         * execution. When a button is clicked, this common handler forwards the event to whatever
         * handler passed in any of the setXXXButton methods, after this code executes dismisses the
         * Dialog.
         *
         * This is a workaround to prevent this native behaviour from happening and show to the user
         * the error when presses the positive button without writing a pin code.
         */
        final AlertDialog alertDialog = (AlertDialog) getDialog();
        if (alertDialog != null) {
            Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(v -> {
                mInputLayout.setError(null);

                CharSequence pinString = mEditTextPin.getEditableText();
                if (TextUtils.isEmpty(pinString)) {
                    //mInputLayout.setError(getString(R.string.pin_input_button_error));
                    mInputLayout.setError("Ingrese un termino de busqueda valido");

                    //noinspection UnnecessaryReturnStatement
                    return;
                }
                mCallbacks.onQuery(pinString.toString());
                alertDialog.dismiss();
            });
        }
    }
}
