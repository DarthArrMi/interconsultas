package org.homi.interconsultas.presentation.dashboard;

import org.homi.interconsultas.base.BaseContract;
import org.homi.interconsultas.presentation.dashboard.domain.Interconsulta;

import java.util.List;


/**
 * @author miguel.arroyo (miguel.arroyo@movile.com).
 */
public interface DashboardContract {


    interface InterconsultaView extends BaseContract.BaseView {

        void getListInterconsulta(List<Interconsulta> list);
    }

    interface Presenter<V extends InterconsultaView> extends BaseContract.BasePresenter {

        void getInterconsulta(String code);
    }
}
