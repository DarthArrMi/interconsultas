package org.homi.interconsultas.presentation.dashboard.search;

import org.homi.interconsultas.presentation.dashboard.domain.Interconsulta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by miguelarroyo on 11/13/17.
 */

public class SearchContext {

    SearchStrategy mSearchStrategy;

    public void setSearchStrategy(SearchStrategy mSearchStrategy) {
        this.mSearchStrategy = mSearchStrategy;
    }

    public ArrayList<Interconsulta> performFiltering(CharSequence query, List<Interconsulta> interconsultas) {
        return mSearchStrategy.performFiltering(query, interconsultas);
    }
}
