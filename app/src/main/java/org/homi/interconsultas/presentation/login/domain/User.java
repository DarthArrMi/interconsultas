package org.homi.interconsultas.presentation.login.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by miguelarroyo on 8/24/17.
 */

public class User implements Parcelable {

    @SerializedName("ESTADO")
    @Expose
    private String mEstado;

    @SerializedName("NOMBRE")
    @Expose
    private String mNombre;

    @SerializedName("NOMBRE_ESPECIALIDAD")
    @Expose
    private String mNombreEspecialidad;

    @SerializedName("CODIGO_ESPECIALIDAD")
    @Expose
    private String mCodigoEspecialidad;

    public User() {
    }

    public String getEstado() {
        return mEstado;
    }

    public void setEstado(String eSTADO) {
        this.mEstado = eSTADO;
    }

    public String getNombre() {
        return mNombre;
    }

    public void setNombre(String nOMBRE) {
        this.mNombre = nOMBRE;
    }

    public String getNombreEspecialidad() {
        return mNombreEspecialidad;
    }

    public void setNombreEspecialidad(String nOMBREESPECIALIDAD) {
        this.mNombreEspecialidad = nOMBREESPECIALIDAD;
    }

    public String getCodigoEspecialidad() {
        return mCodigoEspecialidad;
    }

    public void setCodigoEspecialidad(String cODIGOESPECIALIDAD) {
        this.mCodigoEspecialidad = cODIGOESPECIALIDAD;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (mEstado != null ? !mEstado.equals(user.mEstado) : user.mEstado != null) return false;
        if (mNombre != null ? !mNombre.equals(user.mNombre) : user.mNombre != null) return false;
        if (mNombreEspecialidad != null ? !mNombreEspecialidad.equals(user.mNombreEspecialidad) : user.mNombreEspecialidad != null)
            return false;
        return mCodigoEspecialidad != null ? mCodigoEspecialidad.equals(user.mCodigoEspecialidad) : user.mCodigoEspecialidad == null;

    }

    @Override
    public int hashCode() {
        int result = mEstado != null ? mEstado.hashCode() : 0;
        result = 31 * result + (mNombre != null ? mNombre.hashCode() : 0);
        result = 31 * result + (mNombreEspecialidad != null ? mNombreEspecialidad.hashCode() : 0);
        result = 31 * result + (mCodigoEspecialidad != null ? mCodigoEspecialidad.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("mEstado='").append(mEstado).append('\'');
        sb.append(", mNombre='").append(mNombre).append('\'');
        sb.append(", mNombreEspecialidad='").append(mNombreEspecialidad).append('\'');
        sb.append(", mCodigoEspecialidad='").append(mCodigoEspecialidad).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mEstado);
        dest.writeString(this.mNombre);
        dest.writeString(this.mNombreEspecialidad);
        dest.writeString(this.mCodigoEspecialidad);
    }

    protected User(Parcel in) {
        this.mEstado = in.readString();
        this.mNombre = in.readString();
        this.mNombreEspecialidad = in.readString();
        this.mCodigoEspecialidad = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
