package org.homi.interconsultas.presentation.login.view;

import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.homi.interconsultas.R;
import org.homi.interconsultas.presentation.dashboard.view.DashboardActivity;
import org.homi.interconsultas.presentation.dialogs.LoadingDialog;
import org.homi.interconsultas.presentation.login.LoginContract.LoginView;
import org.homi.interconsultas.presentation.login.presenter.LoginPresenter;

public class LoginActivity extends AppCompatActivity implements LoginView {

    private EditText mTextUserName;
    private EditText mTextPassword;
    private Button mButton;
    private View mContainer;

    LoadingDialog mLoadingDialog;

    private LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mTextUserName = (EditText) findViewById(R.id.login_text_username);
        mTextPassword = (EditText) findViewById(R.id.login_text_password);
        mButton = (Button) findViewById(R.id.login_button_action);
        mContainer = findViewById(R.id.login_container);

        mButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                String username = mTextUserName.getEditableText().toString();
                String password = mTextPassword.getEditableText().toString();

                mLoginPresenter.login(username, password);
            }
        });

        mLoginPresenter = new LoginPresenter();
        mLoginPresenter.attachView(this);
    }

    @Override
    public void showProgressBar() {
        mLoadingDialog = LoadingDialog.newInstance(getString(R.string.dialog_loading_login));
        mLoadingDialog.show(getSupportFragmentManager(), LoadingDialog.FRAGMENT_TAG);
    }

    @Override
    public void hideProgressBar() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(final String errorMessage) {
        Snackbar.make(mContainer, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(@StringRes final int stringResourceId) {
        Snackbar.make(mContainer, stringResourceId, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void loginSuccessful() {
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }
}
