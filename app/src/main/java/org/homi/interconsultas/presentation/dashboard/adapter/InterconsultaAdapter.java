package org.homi.interconsultas.presentation.dashboard.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.homi.interconsultas.R;
import org.homi.interconsultas.presentation.dashboard.domain.Interconsulta;
import org.homi.interconsultas.presentation.dashboard.search.BedStrategy;
import org.homi.interconsultas.presentation.dashboard.search.SearchContext;
import org.homi.interconsultas.presentation.dashboard.search.SpecialtyStrategy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by miguelarroyo on 8/25/17.
 */

public class InterconsultaAdapter extends Adapter<InterconsultaAdapter.ViewHolder> implements Filterable {

    private Context mContext;
    private List<Interconsulta> mConsultas;
    private boolean isFilter;
    private List<Interconsulta> filtered;
    private SearchContext mSearchContext;


    public InterconsultaAdapter(Context mContext) {
        this.mContext = mContext;
        mConsultas = new ArrayList<>();
        filtered = new ArrayList<>();
        mSearchContext = new SearchContext();
    }

    public InterconsultaAdapter(@NonNull Context mContext, @NonNull List<Interconsulta> mConsultas) {
        this.mContext = mContext;
        this.mConsultas = mConsultas;
        filtered = new ArrayList<>();
        mSearchContext = new SearchContext();
    }

    @Override
    public InterconsultaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_interconsulta, parent, false);
        return new InterconsultaAdapter.ViewHolder(view);
    }

    public void addItems(List<Interconsulta> interconsultas) {
        mConsultas.addAll(interconsultas);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(InterconsultaAdapter.ViewHolder holder, int position) {
        Interconsulta interconsulta;
        if (isFilter){
            interconsulta = filtered.get(position);
        }else{
            interconsulta = mConsultas.get(position);
        }


        holder.mTextViewPatient.setText(interconsulta.getPatientName());
        holder.mTextViewHc.setText(interconsulta.getHc());
        holder.mTextViewBed.setText(interconsulta.getBed());
        holder.mTextViewSubspecialty.setText(interconsulta.getSubspecialty());

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-d kk:mm:ss", Locale.getDefault());
        holder.mTextViewDate.setText(dateFormat.format(interconsulta.getDate()));
    }

    @Override
    public int getItemCount() {
        if (isFilter){
            return filtered != null ? filtered.size() : 0;
        }else{
            return mConsultas != null ? mConsultas.size() : 0;
        }

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                filtered = mSearchContext.performFiltering(charSequence, mConsultas);

                FilterResults filterResults = new FilterResults();
                filterResults.values = filtered;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                isFilter = true;
                notifyDataSetChanged();
            }
        };
    }

    public void setSearchStrategy(String query, int type) {
        if (!TextUtils.isEmpty(query)) {
            switch (type) {
                case R.id.search_sheet_bed:
                    mSearchContext.setSearchStrategy(new BedStrategy());
                    break;
                case R.id.search_sheet_specialty:
                    mSearchContext.setSearchStrategy(new SpecialtyStrategy());
                    break;
            }

            getFilter().filter(query);
        } else {
            resetList();
        }
    }

    public void resetList() {
        if (isFilter) {
            isFilter = false;
            notifyDataSetChanged();
        }
    }

    public boolean isFilter() {
        return isFilter;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTextViewPatient;
        TextView mTextViewHc;
        TextView mTextViewBed;
        TextView mTextViewDate;
        TextView mTextViewSubspecialty;

        ViewHolder(View itemView) {
            super(itemView);

            mTextViewPatient = (TextView) itemView.findViewById(R.id.interconsulta_patient);
            mTextViewHc = (TextView) itemView.findViewById(R.id.interconsulta_hc);
            mTextViewBed = (TextView) itemView.findViewById(R.id.interconsulta_bed);
            mTextViewDate = (TextView) itemView.findViewById(R.id.interconsulta_date);
            mTextViewSubspecialty = (TextView) itemView.findViewById(R.id.interconsulta_subspecialty);
        }
    }
}