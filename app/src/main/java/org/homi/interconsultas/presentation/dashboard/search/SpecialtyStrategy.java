package org.homi.interconsultas.presentation.dashboard.search;

import org.homi.interconsultas.presentation.dashboard.domain.Interconsulta;

import java.util.ArrayList;
import java.util.List;

public class SpecialtyStrategy implements SearchStrategy {

    @Override
    public ArrayList<Interconsulta> performFiltering(CharSequence query, List<Interconsulta> interconsultas) {

        ArrayList<Interconsulta> result = new ArrayList<>();
        for(int i = 0;i<interconsultas.size();i++)
        {
            Interconsulta inter = interconsultas.get(i);
            if (inter.getSubspecialty().contains(query)){
                result.add(inter);
            }
        }

        return result;
    }
}
