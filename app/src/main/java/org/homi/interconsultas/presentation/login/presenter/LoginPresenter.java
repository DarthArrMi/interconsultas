package org.homi.interconsultas.presentation.login.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import org.homi.interconsultas.R;
import org.homi.interconsultas.base.BaseContract;
import org.homi.interconsultas.presentation.login.LoginContract.LoginView;
import org.homi.interconsultas.presentation.login.LoginContract.Presenter;
import org.homi.interconsultas.presentation.login.domain.User;
import org.homi.interconsultas.network.ApiService;
import org.homi.interconsultas.network.ApiService.Factory;
import org.homi.interconsultas.utils.PreferenceUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author miguel.arroyo (miguel.arroyo@movile.com).
 */
public class LoginPresenter implements Presenter<LoginView> {

    private final String TAG = LoginPresenter.class.getSimpleName();

    private ApiService mApiService;
    private LoginView mLoginView;
    private PreferenceUtil mPreferenceUtil;

    public LoginPresenter() {
        mApiService = Factory.create();
    }

    @Override
    public void attachView(final BaseContract.BaseView view) {
        mLoginView = (LoginView) view;
        mPreferenceUtil = new PreferenceUtil((Context) mLoginView);
    }

    @Override
    public void detachView() {
        mLoginView = null;
    }

    @Override
    public void login(final String username, final String password) {
        mLoginView.showProgressBar();
        if (TextUtils.isEmpty(username)) {
            mLoginView.showError(R.string.login_error_username);
            return;
        }
        if (TextUtils.isEmpty(password)) {
            mLoginView.showError(R.string.login_error_password);
            return;
        }

        Call<User> call = mApiService.login(username, password);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                mLoginView.hideProgressBar();

                User user = response.body();
                if (user != null && user.getEstado().equals("0")) {
                    mLoginView.showError(R.string.login_error_general);
                } else {
                    setupTopic(user);

                    // Views are always implemented by activities, therefore, it's safe to make this
                    // casting
                    mPreferenceUtil.writeString("USER", new Gson().toJson(user));
                    mLoginView.loginSuccessful();
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                Log.e(TAG, "Error in login", t);
                mLoginView.hideProgressBar();
                mLoginView.showError(R.string.login_error_connection);
            }
        });
    }

    private void setupTopic(User newUser) {
        FirebaseMessaging firebaseMessaging = FirebaseMessaging.getInstance();
        String currentUserJson = mPreferenceUtil.readString("USER");

        if (!TextUtils.isEmpty(currentUserJson)) {
            User currentUser = new Gson().fromJson(currentUserJson, User.class);

            if (currentUser != null) {
                firebaseMessaging.unsubscribeFromTopic(currentUser.getCodigoEspecialidad());
            }
        }

        firebaseMessaging.subscribeToTopic(newUser.getCodigoEspecialidad());
    }
}
