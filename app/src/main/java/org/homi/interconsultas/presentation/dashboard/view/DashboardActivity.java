package org.homi.interconsultas.presentation.dashboard.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioGroup;

import com.google.gson.Gson;

import org.homi.interconsultas.R;
import org.homi.interconsultas.presentation.dashboard.DashboardContract.InterconsultaView;
import org.homi.interconsultas.presentation.dashboard.adapter.InterconsultaAdapter;
import org.homi.interconsultas.presentation.dashboard.domain.Interconsulta;
import org.homi.interconsultas.presentation.dashboard.presenter.DashboardPresenter;
import org.homi.interconsultas.presentation.dialogs.LoadingDialog;
import org.homi.interconsultas.presentation.login.domain.User;
import org.homi.interconsultas.utils.PreferenceUtil;

import java.util.List;

import static android.support.design.widget.BottomSheetBehavior.STATE_EXPANDED;
import static android.support.design.widget.BottomSheetBehavior.STATE_HIDDEN;

/**
 * @author miguel.arroyo (miguel.arroyo@movile.com).
 */
public class DashboardActivity extends AppCompatActivity
        implements InterconsultaView, OnRefreshListener {

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private InterconsultaAdapter mInterconsultaAdapter;

    private DashboardPresenter mDashbordPresenter;
    private LoadingDialog mLoadingDialog;
    // private SearchDialog mSearchDialog;

    private BottomSheetBehavior mBehavior;
    private FloatingActionButton mFabSearch;
    private RadioGroup mRadioGroup;
    private Snackbar mSnackClearSearch;
    private TextInputEditText mTextSearchQuery;
    private TextInputLayout mTextInputSearch;
    private View mContainer;
    private View mSearchContainer;

    private User mUser;

    @Override
    public void onBackPressed() {
        if (mBehavior.getState() == STATE_EXPANDED) {
            mBehavior.setState(STATE_HIDDEN);
            mFabSearch.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mTextInputSearch = (TextInputLayout) findViewById(R.id.pin_input_container_pin);
        mTextSearchQuery = (TextInputEditText) findViewById(R.id.pin_input_pin);
        mFabSearch = (FloatingActionButton) findViewById(R.id.reedem_coupon_fab);
        mRadioGroup = (RadioGroup) findViewById(R.id.search_radio_type);
        mRecyclerView = (RecyclerView) findViewById(R.id.dashboard_recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.dashboard_swipe_to_refresh);
        mSearchContainer = findViewById(R.id.search_dialog_root);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSnackClearSearch = Snackbar.make(mRecyclerView, "", Snackbar.LENGTH_INDEFINITE).setAction(R.string.search_action_clear_list, v -> mInterconsultaAdapter.setSearchStrategy(null, 0));
        mBehavior = BottomSheetBehavior.from(mSearchContainer);
        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == STATE_HIDDEN) {
                    mFabSearch.setVisibility(View.GONE);

                    if (mInterconsultaAdapter.isFilter()) {
                        mSnackClearSearch.show();
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        mBehavior.setState(STATE_HIDDEN);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mInterconsultaAdapter = new InterconsultaAdapter(this);
        mRecyclerView.setAdapter(mInterconsultaAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        mDashbordPresenter = new DashboardPresenter();
        mDashbordPresenter.attachView(this);

        mFabSearch.setOnClickListener(v -> {
            String text = mTextSearchQuery.getEditableText().toString();

            if (!TextUtils.isEmpty(text)) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                mTextSearchQuery.setText("");
                mInterconsultaAdapter.setSearchStrategy(text, mRadioGroup.getCheckedRadioButtonId());
            }
        });

        PreferenceUtil preferenceUtil = new PreferenceUtil(this);
        mUser = new Gson().fromJson(preferenceUtil.readString("USER"), User.class);
        setTitle(mUser.getNombreEspecialidad());
        mDashbordPresenter.getInterconsulta(mUser.getCodigoEspecialidad());
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_filter_item) {
            mSnackClearSearch.dismiss();
            mFabSearch.show();
            mBehavior.setState(STATE_EXPANDED);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgressBar() {
        mLoadingDialog = LoadingDialog.newInstance(getString(R.string.dialog_loading_interconsulta));
        mLoadingDialog.show(getSupportFragmentManager(), LoadingDialog.FRAGMENT_TAG);
    }

    @Override
    public void hideProgressBar() {
        mLoadingDialog.dismiss();
    }

    @Override
    public void showError(String errorMessage) {
        Snackbar.make(mContainer, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(@StringRes int stringResourceId) {
        Snackbar.make(mContainer, stringResourceId, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void getListInterconsulta(List<Interconsulta> list) {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        
        mInterconsultaAdapter.addItems(list);
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        mDashbordPresenter.getInterconsulta(mUser.getCodigoEspecialidad());
    }
}
