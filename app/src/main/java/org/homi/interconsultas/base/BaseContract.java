package org.homi.interconsultas.base;

import android.support.annotation.StringRes;

/**
 * @author miguel.arroyo (miguel.arroyo@movile.com).
 */
public interface BaseContract {

    interface BaseView {

        void showProgressBar();

        void hideProgressBar();

        void showError(String errorMessage);

        void showError(@StringRes int stringResourceId);
    }

    interface BasePresenter<View extends BaseView> {

        void attachView(View view);

        void detachView();
    }
}
